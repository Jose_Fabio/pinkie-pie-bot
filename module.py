import logging
from telegram import ChatAction
from derpibooru import Search, sort

logger = logging.getLogger()

class Comandos():

   global limite

   limite = 1

   def clop(bot, update, args):
        if update.message.chat.id < 0:
           # logger.info("Comando Clop bloqueado")
           bot.delete_message(update.message.chat.id, update.message.message_id)
        else:
           if len(args) == 0:
              # logger.info("Comando Clop recivido")
              for post in Search(filter_id='56027').query("explicit").sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  # logger.info("Busqueda {} enviada".format(post.url))
           elif len(args) == 1:
              foo = args[0]
              foo = str(foo)
              # logger.info("Comando Clop: {} recivido".format(foo))
              for post in Search(filter_id='56027').query("explicit", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  # logger.info("Busqueda {} enviada".format(foo))

   def tentacles(bot, update, args):
        if update.message.chat.id < 0:
           # logger.info("Comando Tentacles bloqueado")
           bot.delete_message(update.message.chat.id, update.message.message_id)
        else:
           if len(args) == 0:
              # logger.info("Comando Tentacles recivido")
              for post in Search(filter_id='56027').query("explicit", "tentacles").sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  # logger.info("Busqueda {} enviada".format(post.url))
           elif len(args) == 1:
              foo = args[0]
              foo = str(foo)
              # logger.info("Comando Tentacles: {} recivido".format(foo))
              for post in Search(filter_id='56027').query("tentacles", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  # logger.info("Busqueda {} enviada".format(foo))

   def pony(bot, update, args):
        if len(args) == 0:
           logger.info("Comando Pony recivido")
           for post in Search().sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               logger.info("Busqueda {} enviada".format(post.url))
        elif len(args) == 1:
           foo = args[0]
           foo = str(foo)
           if foo == "pinkie":
              bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
              update.message.reply_text('LA MEJOR PONY :3')
              logger.info("EL ROSA FUE INBOCADO")
              for post in Search().query("pinkie pie").sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(foo))
           else:
              info = "Busqueda recivida: {}".format(foo)
              logger.info(info)
              for post in Search().query(foo).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(foo))

   def keyboard(bot, update):
        foo = update.message.text
        if update.message.chat.id > 0:
           if foo == "pinkie pie":
              bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
              update.message.reply_text('LA MEJOR PONY :3')
              logger.info("EL ROSA FUE INBOCADO")
              for post in Search().query(foo).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(foo))
           else:
              info = "Busqueda recivida: {}".format(foo)
              logger.info(info)
              for post in Search().query(foo).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(foo))

   def suggestive(bot, update, args):
        if len(args) == 0:
           # logger.info("Comando Suggestive recivido")
           for post in Search(filter_id='56027').query("suggestive").sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               # logger.info("Busqueda {} enviada".format(post.url))
        elif len(args) == 1:
           foo = args[0]
           foo = str(foo)
           # info = "Busqueda recivida: {}".format(foo)
           # logger.info(info)
           for post in Search(filter_id='56027').query("suggestive", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               # logger.info("Busqueda {} enviada".format(foo))

   def ahegao(bot, update, args):
        if len(args) == 0:
           # logger.info("Comando Ahegao recivido")
           for post in Search().query("ahegao").sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               # logger.info("Busqueda {} enviada".format(post.url))
        elif len(args) == 1:
           foo = args[0]
           foo = str(foo)
           # info = "Busqueda recivida: {}".format(foo)
           # logger.info(info)
           for post in Search().query("ahegao", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               # logger.info("Busqueda {} enviada".format(foo))



   def human(bot, update, args):
        if update.message.chat.id < 0:
           logger.info("Comando HM bloqueado")
           bot.delete_message(update.message.chat.id, update.message.message_id)
        else:
           if len(args) == 0:
              logger.info("Comando HA recivido")
              for post in Search(filter_id='56027').query("human on pony action").sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(post.url))
           elif len(args) == 1:
              foo = args[0]
              foo = str(foo)
              logger.info("Comando HA: {} recivido".format(foo))
              for post in Search(filter_id='56027').query("human on pony action", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(foo))


   def mare(bot, update, args):
        if update.message.chat.id < 0:
           logger.info("Comando HM bloqueado")
           bot.delete_message(update.message.chat.id, update.message.message_id)
        else:
           if len(args) == 0:
              logger.info("Comando HM recivido")
              for post in Search(filter_id='56027').query("human male on mare").sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(post.url))
           elif len(args) == 1:
              foo = args[0]
              foo = str(foo)
              logger.info("Comando HM: {} recivido".format(foo))
              for post in Search(filter_id='56027').query("human male on mare", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  logger.info("Busqueda {} enviada".format(foo))

   def eg18(bot, update, args):
        if update.message.chat.id < 0:
           # logger.info("Comando eg18 bloqueado")
           bot.delete_message(update.message.chat.id, update.message.message_id)
        else:
           if len(args) == 0:
              # logger.info("Comando eg18 recivido")
              for post in Search(filter_id='56027').query("explicit", "equestria girls").sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  # logger.info("Busqueda {} enviada".format(post.url))
           elif len(args) == 1:
              foo = args[0]
              foo = str(foo)
              # logger.info("Comando eg18: {} recivido".format(foo))
              for post in Search(filter_id='56027').query("explicit", "equestria girls", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
                  bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
                  bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
                  # logger.info("Busqueda {} enviada".format(foo))

   def eg(bot, update, args):
        if len(args) == 0:
           logger.info("Comando eg recivido")
           for post in Search().query("equestria girls").sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               logger.info("Busqueda {} enviada".format(post.url))
        elif len(args) == 1:
           foo = args[0]
           foo = str(foo)
           logger.info("Comando eg: {} recivido".format(foo))
           for post in Search().query("equestria girls", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               logger.info("Busqueda {} enviada".format(foo))

   def sock(bot, update, args):
        if len(args) == 0:
           logger.info("Comando Sock recivido")
           for post in Search().query("socks").sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               logger.info("Busqueda {} enviada".format(post.url))
        elif len(args) == 1:
           foo = args[0]
           foo = str(foo)
           info = "Busqueda recivida: {}".format(foo)
           logger.info(info)
           for post in Search().query("socks", "{}".format(foo)).sort_by(sort.RANDOM).limit(limite):
               bot.send_chat_action(update.message.chat.id, ChatAction.UPLOAD_PHOTO)
               bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=post.url, caption=post.url)
               logger.info("Busqueda {} enviada".format(foo))



class Tweak():

   def start(bot, update):
        logger.info("Comando Start recivido")
        bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
        update.message.reply_text("¡HOLA! SOY PINKIE PIE Y ESTOY AQUÍ PARA AYUDAR Y/O PARA MOSTRARTE DIVERTIDAS IMÁGENES DESDE DERPYBOORU\nUSA /help PARA VER MIS COMANDOS.")
        bot.send_sticker(update.message.chat.id, "CAADAgADehcAAnCO7Q4UnizikvZzmgI")

   def limitset(bot, update, args):
        global limite
        if len(args) == 0:
            bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
            update.message.reply_text("El límite de imágenes actual es: {}".format(limite))
            logger.info("Consulta de límite recivida")
        elif len(args) == 1:
            cat = args[0]
            limite = cat
            logger.info("Límite establecido en {}".format(cat))
            bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
            update.message.reply_text("Límite de imágenes establecido en: {}".format(cat))

   def codigo(bot, update):
        bot.send_message(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, text='https://gitlab.com/exequielaxelluna/pinkie-pie-bot')

   def reglas(bot, update):
        re = open('rules.jpg', 'rb')
        bot.send_photo(chat_id=update.message.chat.id, reply_to_message_id=update.message.message_id, photo=re, caption="¡LAS REGLAS!")

   def ayuda(bot, update):
       logger.info("Comando Help recivido")
       bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
       update.message.reply_text(text="Usa /pony + argumento para buscar pony.\nUsa /eg + argumento para buscar Equestria Girl.\nUsa /socks + argumento para buscar pony with socks.\nUsa /eg18 + argumento para buscar EG +18 (chat privado).\nUsa /tentacles + argumento para tentaculos (chat privado).\nUsa /clop + argumento para imágenes NSFW (chat privado).\nUsa /human_with_mare + argumento para HM (chat privado).\nUsa /human_action + argumento para HA (chat privado).\nUsa /suggestive + argumento para buscar suggestive.\nUsa /ahegao + argumento para buscar ahegao.\nUsa /about para informacion de este bot.\nAl usar los comandos sin argumento\nobtendras una imagen al azar.\n\n\nAdministración en grupos (requiere admin)\n\nUsa /ban para banear usuarios.\nUsa /unban para desbanear usuarios.\nUsa /del para borrar mensajes.\nUsa /kick para expulsar usuarios.\nUsa /kickme para auto expulsarse.\nUsa /pin para anclar mensajes.\nUsa /pinmute para anclar mensajes silenciosamente.")

   def about(bot, update):
       logger.info("Comando About recivido")
       bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
       update.message.reply_text("Bot desarrollado en Python por @HeavenlyRainbow\nUsando las API: Python-Telegram-Bot y Derpibooru-API.\nUsa /codigo para ir al repo de este bot.")
       bot.send_sticker(update.message.chat.id, "CAADAgADUBoAAnCO7Q5RA-liJerMUQI")
