#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import os
import sys
import time
import configparser
from threading import Thread
from admin import Control
from module import Comandos, Tweak
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

global error_id
error_id = 00

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()

def error(bot, update, error):
    global error_id
    error_id = error_id + 1
    fail = '''La Solicitud:\n\n "{}"
    		Causó el error:  "{}"\n\nNúmero de ID: "{}"
    		Por favor, reenvie este mensaje al creador: @HeavenlyRainbow'''.format(update, error, error_id)

    fail2 = 'ERROR: {} ID: {} ACTUALIZACION: {}'.format(error, error_id, update)
    logger.warning(fail2)
    update.message.reply_text(fail)

def main():
    logger.info("Iniciando Bot...")

    config = configparser.ConfigParser()
    config.read('bot.ini')

    updater = Updater(token=config['KEYS']['bot_api'])
    dp = updater.dispatcher

    def stop_and_restart():
        updater.stop()
        os.execl(sys.executable, sys.executable, *sys.argv)

    def restart(bot, update):
        update.message.reply_text('Reiniciando....')
        time.sleep(10)
        update.message.reply_text('Bot Reiniciado Correctamente')
        Thread(target=stop_and_restart).start()


    dp.add_handler(CommandHandler("start", Tweak.start))
    dp.add_handler(CommandHandler("rules", Tweak.reglas))
    dp.add_handler(CommandHandler("clop", Comandos.clop, pass_args=True))
    dp.add_handler(CommandHandler("socks", Comandos.sock, pass_args=True))
    dp.add_handler(CommandHandler("tentacles", Comandos.tentacles, pass_args=True))
    dp.add_handler(CommandHandler("about", Tweak.about))
    dp.add_handler(CommandHandler("eg", Comandos.eg, pass_args=True ))
    dp.add_handler(CommandHandler("eg18", Comandos.eg18, pass_args=True ))
    dp.add_handler(CommandHandler("human_action", Comandos.human, pass_args=True ))
    dp.add_handler(CommandHandler("human_with_mare", Comandos.mare, pass_args=True))
    dp.add_handler(CommandHandler("help", Tweak.ayuda))
    dp.add_handler(CommandHandler("codigo", Tweak.codigo))
    dp.add_handler(CommandHandler("pony", Comandos.pony, pass_args=True))
    dp.add_handler(CommandHandler("suggestive", Comandos.suggestive, pass_args=True))
    dp.add_handler(CommandHandler("ahegao", Comandos.ahegao, pass_args=True))
    dp.add_handler(CommandHandler("limit", Tweak.limitset, pass_args=True, filters=Filters.user(username='@HeavenlyRainbow')))
    dp.add_handler(CommandHandler('restart', restart, filters=Filters.user(username='@HeavenlyRainbow')))

    dp.add_handler(CommandHandler("ban", Control.ban, Filters.group))
    dp.add_handler(CommandHandler("del", Control.delete, Filters.group))
    dp.add_handler(CommandHandler("kick", Control.kick, Filters.group))
    dp.add_handler(CommandHandler("kickme", Control.kick_me, Filters.group))
    dp.add_handler(CommandHandler("pin", Control.pin, Filters.group))
    dp.add_handler(CommandHandler("pinmute", Control.pin_mute, Filters.group))
    dp.add_handler(CommandHandler("unban", Control.unban, Filters.group))


    dp.add_handler(MessageHandler(Filters.status_update.new_chat_members, Control.new_chat_members))

    dp.add_handler(MessageHandler(Filters.text, Comandos.keyboard))

    dp.add_error_handler(error)

    logger.info("Handlers añadidos exitosamente")


    updater.start_polling()

    logger.info("Bot Iniciado Correctamente")

    updater.idle()


if __name__ == '__main__':
    main()

