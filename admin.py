import logging
from telegram import ChatAction, ChatMember

logger = logging.getLogger()

class Control():

    def new_chat_members(bot, update):


        if len(update.message.new_chat_members) == 1:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("HELLO, {}, AND WELCOME TO THE BEST GROUP EVER!!!".format(update.message.new_chat_members[0].first_name.upper()))
        else:
           update.message.reply_text("Hola, ¡sean todos bienvenidos a este genial grupo!")

        bot.send_sticker(update.message.chat.id, "CAADAgADiQ8AAnCO7Q5nsNV4TKoEqQI")


    def pin(bot, update):

        logger.info("Comando pin recivido")

        if update.message.reply_to_message is None:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("¿Qué mensaje debo anclar?\n¿Acaso es invisible?")
           bot.send_sticker(update.message.chat.id, "CAADAgADsg0AAnCO7Q7guXdi6oHywwI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if not chat_member.can_pin_messages and not chat_member.status == ChatMember.CREATOR:
            update.message.reply_sticker("CAADAgAD9Q8AAnCO7Q6TKZ-efF_-hgI")
            bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
            bot.send_message(update.message.chat.id, "Tú no puedes anclar mensajes")
            return

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_pin_messages:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        bot.pin_chat_message(update.message.chat.id, update.message.reply_to_message.message_id)

        bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
        update.message.reply_to_message.reply_text("ANCLADO CON EXITO AL ESTILO PINKIE PIE")
        bot.send_sticker(update.message.chat.id, "CAADAgADUBoAAnCO7Q5RA-liJerMUQI")


    def pin_mute(bot, update):

        logger.info("Comando pinmute recivido")

        if update.message.reply_to_message is None:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("¿Qué mensaje debo anclar?\n¿Acaso es invisible?")
           bot.send_sticker(update.message.chat.id, "CAADAgADsg0AAnCO7Q7guXdi6oHywwI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if not chat_member.can_pin_messages and not chat_member.status == ChatMember.CREATOR:
            update.message.reply_sticker("CAADAgAD9Q8AAnCO7Q6TKZ-efF_-hgI")
            bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
            bot.send_message(update.message.chat.id, "Tú no puedes anclar mensajes")
            return

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_pin_messages:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        bot.pin_chat_message(update.message.chat.id, update.message.reply_to_message.message_id, True)


    def delete(bot, update):

        logger.info("Comando del recivido")

        if update.message.reply_to_message is None:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("¿Qué mensaje debo borrar?\n¿Acaso es invisible?")
           bot.send_sticker(update.message.chat.id, "CAADAgADsg0AAnCO7Q7guXdi6oHywwI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if not chat_member.can_delete_messages and not chat_member.status == ChatMember.CREATOR:
            update.message.reply_sticker("CAADAgAD9Q8AAnCO7Q6TKZ-efF_-hgI")
            bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
            bot.send_message(update.message.chat.id, "Tú no puedes borrar mensajes")
            return

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_delete_messages:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        bot.delete_message(update.message.chat.id, update.message.reply_to_message.message_id)
        bot.delete_message(update.message.chat.id, update.message.message_id)

    def ban(bot, update):

        logger.info("Comando ban recivido")

        if update.message.reply_to_message is None:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("No puedo banear a nadie si no respondes a un mensaje")
           bot.send_sticker(update.message.chat.id, "CAADAgADHQwAAnCO7Q5uypGPrQJrSgI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if not chat_member.can_restrict_members and not chat_member.status == ChatMember.CREATOR:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Lo siento pero...\ntú no tienes licencia para usar martillos")
           bot.send_sticker(update.message.chat.id, "CAADAgAD9woAAnCO7Q6rSbr_coRe8wI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_restrict_members:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        if update.message.reply_to_message.forward_from is not None:
           user = update.message.reply_to_message.forward_from
        else:
           user = update.message.reply_to_message.from_user

        if user == bot.get_me():
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("¿ACASO YA NO ME QUIERES?")
           bot.send_sticker(update.message.chat.id, "CAADAgADxxIAAnCO7Q6ZM6879kWrZwI")
           return

        if update.message.reply_to_message.forward_from is not None:
           chat_member = bot.get_chat_member(update.message.chat.id, update.message.reply_to_message.forward_from.id)
        else:
           chat_member = bot.get_chat_member(update.message.chat.id, update.message.reply_to_message.from_user.id)

        if chat_member.status == ChatMember.ADMINISTRATOR or chat_member.status == ChatMember.CREATOR:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("No puedo banear administradores... aún")
           bot.send_sticker(update.message.chat.id, "CAADAgADyBIAAnCO7Q6yJ9tWiFBjHAI")
           return

        if update.message.reply_to_message.forward_from is not None:
           bot.kick_chat_member(update.message.chat.id, update.message.reply_to_message.forward_from.id)
        else:
           bot.kick_chat_member(update.message.chat.id, update.message.reply_to_message.from_user.id)

        bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
        update.message.reply_to_message.reply_text("¡Hasta la vista, baby!")
        bot.send_sticker(update.message.chat.id, "CAADAgAD9BwAAnCO7Q7erjWzH6Rk9AI")


    def kick(bot, update):

        logger.info("Comando kick recivido")

        if update.message.reply_to_message is None:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("No puedo expulsar a nadie si no respondes a un mensaje")
           bot.send_sticker(update.message.chat.id, "CAADAgADHQwAAnCO7Q5uypGPrQJrSgI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if not chat_member.can_restrict_members and not chat_member.status == ChatMember.CREATOR:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Lo siento, pero tú no puedes expulsar usuarios...")
           bot.send_sticker(update.message.chat.id, "CAADAgAD9woAAnCO7Q6rSbr_coRe8wI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_restrict_members:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        if update.message.reply_to_message.forward_from is not None:
           user = update.message.reply_to_message.forward_from
        else:
           user = update.message.reply_to_message.from_user

        if user == bot.get_me():
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("¿ACASO YA NO ME QUIERES?")
           bot.send_sticker(update.message.chat.id, "CAADAgADxxIAAnCO7Q6ZM6879kWrZwI")
           return

        if update.message.reply_to_message.forward_from is not None:
           chat_member = bot.get_chat_member(update.message.chat.id, update.message.reply_to_message.forward_from.id)
        else:
           chat_member = bot.get_chat_member(update.message.chat.id, update.message.reply_to_message.from_user.id)

        if chat_member.status == ChatMember.ADMINISTRATOR or chat_member.status == ChatMember.CREATOR:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("No puedo expulsar administradores... aún")
           bot.send_sticker(update.message.chat.id, "CAADAgADyBIAAnCO7Q6yJ9tWiFBjHAI")
           return

        if update.message.reply_to_message.forward_from is not None:
           bot.kick_chat_member(update.message.chat.id, update.message.reply_to_message.forward_from.id)
           bot.unban_chat_member(update.message.chat.id, update.message.reply_to_message.forward_from.id)
        else:
           bot.kick_chat_member(update.message.chat.id, update.message.reply_to_message.from_user.id)
           bot.unban_chat_member(update.message.chat.id, update.message.reply_to_message.from_user.id)

        bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
        update.message.reply_to_message.reply_text("¡ADIOS!")
        bot.send_sticker(update.message.chat.id, "CAADAgADjQ8AAnCO7Q7PyjT8fVR0nAI")


    def kick_me(bot, update):

        logger.info("Comando kickme recivido")

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_restrict_members:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if chat_member.status == ChatMember.ADMINISTRATOR or chat_member.status == ChatMember.CREATOR:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("No puedo expulsar administradores... aún")
           bot.send_sticker(update.message.chat.id, "CAADAgADyBIAAnCO7Q6yJ9tWiFBjHAI")
           return

        bot.kick_chat_member(update.message.chat.id, update.message.from_user.id)
        bot.unban_chat_member(update.message.chat.id, update.message.from_user.id)

        bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
        update.message.reply_text("¡Espero que vuelvas pronto!")
        bot.send_sticker(update.message.chat.id, "CAADAgAD7QoAAnCO7Q4z90sPbUHCGwI")


    def unban(bot, update):

        logger.info("Comando unban recivido")

        if update.message.reply_to_message is None:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("No puedo desbanear a nadie si no respondes a un mensaje")
           bot.send_sticker(update.message.chat.id, "CAADAgADHQwAAnCO7Q5uypGPrQJrSgI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
        if not chat_member.can_restrict_members and not chat_member.status == ChatMember.CREATOR:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Lo siento, pero tú no puedes debanear usuarios...")
           bot.send_sticker(update.message.chat.id, "CAADAgAD9woAAnCO7Q6rSbr_coRe8wI")
           return

        chat_member = bot.get_chat_member(update.message.chat.id, bot.get_me().id)
        if not chat_member.can_restrict_members:
           bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
           update.message.reply_text("Olvidaste un detalle muy pequeñito...\n¡NO SOY ADMINISTRADORA DEL GRUPO!")
           bot.send_sticker(update.message.chat.id, "CAADAgADGgwAAnCO7Q5IlyOxGJy7RAI")
           return

        if update.message.reply_to_message.forward_from is not None:
           bot.unban_chat_member(update.message.chat.id, update.message.reply_to_message.forward_from.id)
        else:
           bot.unban_chat_member(update.message.chat.id, update.message.reply_to_message.from_user.id)

        bot.send_chat_action(update.message.chat.id, ChatAction.TYPING)
        update.message.reply_to_message.reply_text("¡LISTO!")
        bot.send_sticker(update.message.chat.id, "CAADAgADWyEAAnCO7Q76ASA6bi7ShwI")